# Exercice 2 - Newsletter

##
- HTML
- CSS

## Architecture
- index.html
- readme.md
 - images
    - bague-martelé-verte.png
    - bague-verte.png
    - bracelet-doré-rose.png
    - colliers.png
    - logo-inaya-selene.png

## Consignes
- Inventer une newsletter en faisant la promotion d'une marque, d'un produit
- Réaliser la newsletter avec le système des tableaux en html/css